Ansible Role Nginx
=========

Nginx installation role

Requirements
------------

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

Role Variables
--------------
default file for ansible-role-nginx:

`nginx_configuration_path: "{{ role_path }}/templates/nginx.conf.j2"`

`app_host_ip_address: "31.40.251.72"`

`docker_application_bind_port: "8000"`

`nginx_user: "www-data"`

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: web
      roles:
         - citrulline.ansible_role_nginx

License
-------

BSD

Author Information
------------------

Vladislav Pavlov
